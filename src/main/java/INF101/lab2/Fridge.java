package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridge = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return fridge.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (20>fridge.size()){
            fridge.add(item);
            return true;
        }
        else{
            return false;
        }

    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        
        if (fridge.contains(item)){
            fridge.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }



    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expItems = new ArrayList<>();
        
        for (FridgeItem item : fridge) {
            if (item.hasExpired()){
                expItems.add(item);
                
            }
            
        }

        for (FridgeItem item : expItems){
            takeOut(item);
        }
        
        return expItems;
    }

}
